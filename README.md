# 5 cm Delay Wire Chamber

This board is a delay line implementation with the intent to be used as the analog frontend
in a 5 cm delay wire chamber. The board schematic is based on the
[CERN Delay Wire Chamber User Guide](https://cds.cern.ch/record/702443/files/sl-note-98-023.pdf),
with the obsolete operation amplifiers replaced with modern alternatives and the delay lines
replaced with discrete components. Also, the necessary modifications for a smaller chamber
were made.

![alt text](img/DelayLine4Layer.png "Preview of the DWC Delay Line Board")

A few possible future imporvements:
* The output signals are routed through and close the VSS and VPP planes, yet there is no capacitor
near the output terminals for the return current to flow to the ground. Adding capacitors near the
terminals would allow for optimized return loops

![alt text](img/DelayLine4Layer-Back.png "Preview of the Back of DWC Delay Line Board")
